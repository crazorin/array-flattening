const assert = require("assert");

const flattenArray = (inputArray, accumulatorArray = []) => {
  if(!Array.isArray(inputArray)) {
    return "Please provide an array as input.";
  }

  if(!Array.isArray(accumulatorArray)) {
    return "If you provide a target array, please make sure it is an array.";
  }

  inputArray.forEach(element => {
    if(Array.isArray(element)) {
      return flattenArray(element, accumulatorArray);
    } else {
      accumulatorArray.push(element);
    }
  });
  
  return accumulatorArray;
}

// Tests
assert.strictEqual(flattenArray("string input"), "Please provide an array as input.");
assert.strictEqual(flattenArray(), "Please provide an array as input.");
assert.strictEqual(flattenArray([1, 2], null), "If you provide a target array, please make sure it is an array.");
assert.strictEqual(flattenArray([1, 2], { "id": 1 }), "If you provide a target array, please make sure it is an array.");
assert.deepEqual(flattenArray([1, [2, [3]], 4]), [1, 2, 3, 4], );
assert.deepEqual(flattenArray([1, [2, [3]]], [0]), [0, 1, 2, 3]);
assert.deepEqual(flattenArray([1, [2, [3], [null]], 4, undefined]), [1, 2, 3, null, 4, undefined], );
assert.deepEqual(flattenArray([1, [2, ["3"]], "string"]), [1, 2, "3", "string"]);
assert.deepEqual(flattenArray([{ "id": 1 }, [2, [{ "animal": "lion" }]]]), [{ "id": 1 }, 2, { "animal": "lion" }]);
assert.deepEqual(flattenArray([1, [2, [3]], 4]), [1, 2, 3, 4]);
assert.deepEqual(flattenArray([1, [2, [3]], 4, [[[5]], [6, [[[7]]], 8], 9]]), [1, 2, 3, 4, 5, 6, 7, 8, 9]);
